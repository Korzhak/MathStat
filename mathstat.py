import random

# Генерація рандомних чисел
def generic_random_numb(n):
    return [random.randint(1,5) for i in range(10+n)]

class MathStat:
    def __init__(self, lst):
        # Довжина списку
        self.n = len(lst)
        # Вхідні дані
        self.lst = lst

        # Варіаційний ряд
        self.var_series = lst.copy(); self.var_series.sort()

        # Варіанти
        self.varianta = self.varianta(self.var_series)

        # Диференціальні абсолютні частоти
        self.daf = self.diff_abs_frq(self.var_series, self.varianta)

        # Диференціальні відносні частоти
        self.drf = self.diff_rel_frq(self.daf, self.n)

        # Інтегральні абсолютні частоти
        self.iaf = self.int_abs_frq(self.daf)

        # Інтегральні відносні частоти
        self.irf = self.int_rel_frq(self.iaf, self.n)

        # Медіана
        self.mediana = self.mediana(self.var_series)

        # Мода
        self.moda = self.moda(self.daf)

        # Середнє значення
        self.hv = self.half_val() 

    def varianta(self, var_series):
        lst = []
        for var in var_series:
            if not var in lst:
                lst.append(var)
        return lst

    def diff_abs_frq(self, var_series, variants):
        dictnr = {}
        for varianta in variants:
            dictnr[varianta] = var_series.count(varianta)
        return dictnr

    def diff_rel_frq(self, diff_abs_frq, n):
        dictnr = {}
        for m in diff_abs_frq:
            dictnr[m] = round(m / n, 2)
        return dictnr

    def int_abs_frq(self, diff_abs_frq):
        dictnr = {}
        count = 0
        for m in diff_abs_frq:
            count += diff_abs_frq[m]
            dictnr[m] =  count
        return dictnr

    def int_rel_frq(self, int_abs_frq, n):
        dictnr = {}
        for m in int_abs_frq:
            dictnr[m] = round(int_abs_frq[m] / n, 2)
        return dictnr

    def mediana(self, var_series):
        lent = len(var_series)
        if lent % 2 == 0:
            return (var_series[int(lent/2)-1] + var_series[int(lent/2)]) / 2
        else:
            return var_series[int(lent/2)]
    
    def moda(self, diff_abs_frq):
        return diff_abs_frq.pop(max(diff_abs_frq.values()))

    def half_val(self):
        return sum(self.lst) / self.n
            
    def __call__(self):
        return self.lst

    def __str__(self):
        return str(self.lst)

